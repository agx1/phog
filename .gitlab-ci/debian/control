Source: phog
Section: x11
Priority: optional
Maintainer: DebianOnMobile Maintainers <debian-on-mobile-maintainers@alioth-lists.debian.net>
Uploaders: Arnaud Ferraris <aferraris@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 gtk-doc-tools <!nodoc>,
 libgcr-3-dev,
 libglib2.0-dev (>= 2.72.0),
 libglib2.0-doc <!nodoc>,
 libgnome-desktop-3-dev,
 libgtk-3-dev,
 libgtk-3-doc <!nodoc>,
 libgudev-1.0-dev,
 libjson-glib-dev,
 libhandy-1-dev (>= 1.1.90),
 libnm-dev,
 libpam0g-dev,
 libsystemd-dev,
 libupower-glib-dev,
 libwayland-dev,
 meson,
 pandoc <!nodoc>,
# to run the tests
 at-spi2-core <!nocheck>,
 dbus-x11 <!nocheck>,
 gnome-settings-daemon-common <!nocheck>,
 gnome-shell-common <!nocheck>,
 gnome-themes-extra-data <!nocheck>,
 gsettings-desktop-schemas <!nocheck>,
 phoc (>= 0.20.0) <!nocheck>,
 xvfb <!nocheck>,
 xauth <!nocheck>,
Standards-Version: 4.6.1
Homepage: https://gitlab.com/mobian1/phog/
Vcs-Browser: https://salsa.debian.org/DebianOnMobile-team/phog
Vcs-Git: https://salsa.debian.org/DebianOnMobile-team/phog.git
Rules-Requires-Root: no

Package: phog
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 fonts-lato,
 gnome-shell-common,
 gsettings-desktop-schemas,
 phoc (>= 0.21.0+ds1),
 squeekboard,
Recommends:
 slurp,
 squeekboard,
Breaks:
 gnome-control-center (<< 42),
 libgtk-3-0 (<< 3.24.30),
Description: Greetd-compatible greeter for mobile phones
 Phog is a graphical greeter speaking the `greetd` protocol and aimed at mobile
 devices like smart phones and tablets using touch based inputs and small
 screens.
 .
 It was initially designed for the Phosh Mobile Environment based on GNOME/GTK
 but can spawn any graphical session.

Package: phog-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Build-Profiles: <!nodoc>
Depends:
 ${misc:Depends},
Description: Greetd-compatible greeter for mobile phones - documentation
 Phog is a graphical greeter speaking the `greetd` protocol and aimed at mobile
 devices like smart phones and tablets using touch based inputs and small
 screens.
 .
 This package contains the development documentation.
