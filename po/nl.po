# Joachim Moernaut <joachim@mailfence.com>, 2018. #zanata
# Joachim Moernaut <joachim@mailfence.com>, 2019. #zanata
# Willem Sonke <willem@wimiso.nl>, 2019. #zanata
# Jan Jasper de Kroon <jajadekroon@gmail.com>, 2021-2022.
# Nathan Follens <nfollens@gnome.org>, 2021-2022.
msgid ""
msgstr ""
"Project-Id-Version: phosh\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Phosh/phosh/issues\n"
"POT-Creation-Date: 2022-06-01 08:28+0000\n"
"PO-Revision-Date: 2022-06-09 18:37+0200\n"
"Last-Translator: Philip Goto <philip.goto@gmail.com>\n"
"Language-Team: Dutch\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1\n"

#. Translators: this is the session name, no need to translate it
#: data/phosh.session.desktop.in.in:4
msgid "Phosh"
msgstr "Phosh"

#: data/sm.puri.Phosh.desktop.in.in:4
msgid "Phone Shell"
msgstr "Telefoonshell"

#: data/sm.puri.Phosh.desktop.in.in:5
msgid "Window management and application launching for mobile"
msgstr "Vensterbeheer en opstarten van mobiele toepassingen"

#: src/app-grid-button.c:529
msgid "Application"
msgstr "Toepassing"

#: src/app-grid.c:137
msgid "Show All Apps"
msgstr "Alle apps weergeven"

#: src/app-grid.c:140
msgid "Show Only Mobile Friendly Apps"
msgstr "Alleen mobielvriendelijke apps weergeven"

#: src/bt-info.c:92 src/feedbackinfo.c:78 src/rotateinfo.c:103
msgid "On"
msgstr "Aan"

#: src/bt-info.c:94
msgid "Bluetooth"
msgstr "Bluetooth"

#: src/docked-info.c:81
msgid "Docked"
msgstr "In dock"

#: src/docked-info.c:81 src/docked-info.c:199
msgid "Undocked"
msgstr "Uit dock"

#: src/end-session-dialog.c:162
msgid "Log Out"
msgstr "Uitloggen"

#: src/end-session-dialog.c:165
#, c-format
msgid "%s will be logged out automatically in %d second."
msgid_plural "%s will be logged out automatically in %d seconds."
msgstr[0] "%s wordt automatisch uitgelogd over %d seconde."
msgstr[1] "%s wordt automatisch uitgelogd over %d seconden."

#: src/end-session-dialog.c:171 src/ui/top-panel.ui:36
msgid "Power Off"
msgstr "Uitschakelen"

#: src/end-session-dialog.c:172
#, c-format
msgid "The system will power off automatically in %d second."
msgid_plural "The system will power off automatically in %d seconds."
msgstr[0] "Het systeem wordt automatisch uitgeschakeld over %d seconde."
msgstr[1] "Het systeem wordt automatisch uitgeschakeld over %d seconden."

#: src/end-session-dialog.c:178 src/ui/top-panel.ui:29
msgid "Restart"
msgstr "Herstarten"

#: src/end-session-dialog.c:179
#, c-format
msgid "The system will restart automatically in %d second."
msgid_plural "The system will restart automatically in %d seconds."
msgstr[0] "Het systeem zal automatisch herstarten over %d seconde."
msgstr[1] "Het systeem zal automatisch herstarten over %d seconden."

#: src/end-session-dialog.c:269
msgid "Unknown application"
msgstr "Onbekende applicatie"

#. Translators: quiet and silent are fbd profiles names:
#. see https://source.puri.sm/Librem5/feedbackd#profiles
#. for details
#: src/feedbackinfo.c:69
msgid "Quiet"
msgstr "Rustig"

#. Translators: quiet and silent are fbd profiles names:
#. see https://source.puri.sm/Librem5/feedbackd#profiles
#. for details
#: src/feedbackinfo.c:75
msgid "Silent"
msgstr "Stil"

#: src/location-manager.c:268
#, c-format
msgid "Allow '%s' to access your location information?"
msgstr "‘%s’ toegang geven tot uw locatiegegevens?"

#: src/location-manager.c:273
msgid "Geolocation"
msgstr "Geolocatie"

#: src/location-manager.c:274
msgid "Yes"
msgstr "Ja"

#: src/location-manager.c:274
msgid "No"
msgstr "Nee"

#: src/lockscreen.c:165 src/ui/lockscreen.ui:181
msgid "Enter Passcode"
msgstr "Voer de pincode in"

#: src/lockscreen.c:364
msgid "Checking…"
msgstr "Bezig met controleren…"

#. Translators: Used when the title of a song is unknown
#: src/media-player.c:322 src/ui/media-player.ui:182
msgid "Unknown Title"
msgstr "Onbekende titel"

#. Translators: Used when the artist of a song is unknown
#: src/media-player.c:330 src/ui/media-player.ui:165
msgid "Unknown Artist"
msgstr "Onbekende artiest"

#: src/monitor-manager.c:119
msgid "Built-in display"
msgstr "Ingebouwd beeldscherm"

#: src/monitor-manager.c:137
#, c-format
msgctxt ""
"This is a monitor vendor name, followed by a size in inches, like 'Dell 15\"'"
msgid "%s %s"
msgstr "%s %s"

#: src/monitor-manager.c:144
#, c-format
msgctxt ""
"This is a monitor vendor name followed by product/model name where size in "
"inches could not be calculated, e.g. Dell U2414H"
msgid "%s %s"
msgstr "%s %s"

#. Translators: An unknown monitor type
#: src/monitor-manager.c:153
msgid "Unknown"
msgstr "Onbekend"

#: src/network-auth-prompt.c:201
#, c-format
msgid "Authentication type of wifi network “%s” not supported"
msgstr "Authenticatietype van wifinetwerk ‘%s’ wordt niet ondersteund"

#: src/network-auth-prompt.c:206
#, c-format
msgid "Enter password for the wifi network “%s”"
msgstr "Voer het wachtwoord in voor wifinetwerk ‘%s’"

#: src/notifications/mount-notification.c:122
msgid "Open"
msgstr "Openen"

#: src/notifications/notification.c:383 src/notifications/notification.c:639
msgid "Notification"
msgstr "Notificatie"

#. Translators: Timestamp seconds suffix
#: src/notifications/timestamp-label.c:84
msgctxt "timestamp-suffix-seconds"
msgid "s"
msgstr "s"

#. Translators: Timestamp minute suffix
#: src/notifications/timestamp-label.c:86
msgctxt "timestamp-suffix-minute"
msgid "m"
msgstr "m"

#. Translators: Timestamp minutes suffix
#: src/notifications/timestamp-label.c:88
msgctxt "timestamp-suffix-minutes"
msgid "m"
msgstr "m"

#. Translators: Timestamp hour suffix
#: src/notifications/timestamp-label.c:90
msgctxt "timestamp-suffix-hour"
msgid "h"
msgstr "u"

#. Translators: Timestamp hours suffix
#: src/notifications/timestamp-label.c:92
msgctxt "timestamp-suffix-hours"
msgid "h"
msgstr "u"

#. Translators: Timestamp day suffix
#: src/notifications/timestamp-label.c:94
msgctxt "timestamp-suffix-day"
msgid "d"
msgstr "d"

#. Translators: Timestamp days suffix
#: src/notifications/timestamp-label.c:96
msgctxt "timestamp-suffix-days"
msgid "d"
msgstr "d"

#. Translators: Timestamp month suffix
#: src/notifications/timestamp-label.c:98
msgctxt "timestamp-suffix-month"
msgid "mo"
msgstr "mnd"

#. Translators: Timestamp months suffix
#: src/notifications/timestamp-label.c:100
msgctxt "timestamp-suffix-months"
msgid "mos"
msgstr "mnd"

#. Translators: Timestamp year suffix
#: src/notifications/timestamp-label.c:102
msgctxt "timestamp-suffix-year"
msgid "y"
msgstr "j"

#. Translators: Timestamp years suffix
#: src/notifications/timestamp-label.c:104
msgctxt "timestamp-suffix-years"
msgid "y"
msgstr "j"

#: src/notifications/timestamp-label.c:121
msgid "now"
msgstr "nu"

#. Translators: time difference "Over 5 years"
#: src/notifications/timestamp-label.c:189
#, c-format
msgid "Over %dy"
msgstr "Meer dan %dj"

#. Translators: time difference "almost 5 years"
#: src/notifications/timestamp-label.c:193
#, c-format
msgid "Almost %dy"
msgstr "Bijna %dj"

#. Translators: a time difference like '<5m', if in doubt leave untranslated
#: src/notifications/timestamp-label.c:200
#, c-format
msgid "%s%d%s"
msgstr "%s%d%s"

#: src/polkit-auth-agent.c:228
msgid "Authentication dialog was dismissed by the user"
msgstr "Authenticatievenster is door de gebruiker afgesloten"

#: src/polkit-auth-prompt.c:278 src/ui/gtk-mount-prompt.ui:20
#: src/ui/network-auth-prompt.ui:82 src/ui/polkit-auth-prompt.ui:56
#: src/ui/system-prompt.ui:32
msgid "Password:"
msgstr "Wachtwoord:"

#: src/polkit-auth-prompt.c:325
msgid "Sorry, that didn’t work. Please try again."
msgstr "Helaas, dat werkte niet. Probeer het opnieuw."

#: src/rotateinfo.c:81
msgid "Portrait"
msgstr "Staand"

#: src/rotateinfo.c:84
msgid "Landscape"
msgstr "Liggend"

#. Translators: Automatic screen orientation is either on (enabled) or off (locked/disabled)
#. Translators: Automatic screen orientation is off (locked/disabled)
#: src/rotateinfo.c:103 src/rotateinfo.c:186
msgid "Off"
msgstr "Uit"

#: src/run-command-dialog.c:129
msgid "Press ESC to close"
msgstr "Druk op ESC om te sluiten"

#: src/run-command-manager.c:94
#, c-format
msgid "Running '%s' failed"
msgstr "Uitvoeren van ‘%s’ mislukt"

#: src/system-prompt.c:365
msgid "Passwords do not match."
msgstr "Wachtwoorden komen niet overeen."

#: src/system-prompt.c:372
msgid "Password cannot be blank"
msgstr "Wachtwoord mag niet leeg zijn"

#: src/torch-info.c:80
msgid "Torch"
msgstr "Zaklamp"

#: src/ui/app-auth-prompt.ui:49
msgid "Remember decision"
msgstr "Keuze onthouden"

#: src/ui/app-auth-prompt.ui:62 src/ui/end-session-dialog.ui:53
msgid "Cancel"
msgstr "Annuleren"

#: src/ui/app-auth-prompt.ui:71 src/ui/end-session-dialog.ui:62
msgid "Ok"
msgstr "Oké"

#: src/ui/app-grid-button.ui:55
msgid "App"
msgstr "App"

#: src/ui/app-grid-button.ui:79
msgid "Remove from _Favorites"
msgstr "Verwijderen uit _favorieten"

#: src/ui/app-grid-button.ui:84
msgid "Add to _Favorites"
msgstr "Toevoegen aan _favorieten"

#: src/ui/app-grid-button.ui:89
msgid "View _Details"
msgstr "_Details bekijken"

#: src/ui/app-grid.ui:21
msgid "Search apps…"
msgstr "Apps zoeken…"

#: src/ui/end-session-dialog.ui:31
msgid "Some applications are busy or have unsaved work"
msgstr "Sommige applicaties zijn bezig of hebben niet-opgeslagen werk"

#: src/ui/gtk-mount-prompt.ui:94
msgid "User:"
msgstr "Gebruikersnaam:"

#: src/ui/gtk-mount-prompt.ui:117
msgid "Domain:"
msgstr "Domein:"

#: src/ui/gtk-mount-prompt.ui:150
msgid "Co_nnect"
msgstr "Verbi_nden"

#: src/ui/lockscreen.ui:42
msgid "Slide up to unlock"
msgstr "Veeg omhoog om te ontgrendelen"

#: src/ui/lockscreen.ui:231
msgid "Emergency"
msgstr "Noodgeval"

#: src/ui/lockscreen.ui:247
msgid "Unlock"
msgstr "Ontgrendelen"

#: src/ui/lockscreen.ui:289
msgid "Back"
msgstr "Terug"

#: src/ui/network-auth-prompt.ui:5 src/ui/polkit-auth-prompt.ui:6
msgid "Authentication required"
msgstr "Verificatie vereist"

#: src/ui/network-auth-prompt.ui:40
msgid "_Cancel"
msgstr "_Annuleren"

#: src/ui/network-auth-prompt.ui:58
msgid "C_onnect"
msgstr "_Verbinden"

#: src/ui/polkit-auth-prompt.ui:122
msgid "Authenticate"
msgstr "Aanmelden"

#: src/ui/run-command-dialog.ui:6
msgid "Run Command"
msgstr "Opdracht uitvoeren"

#: src/ui/settings.ui:317
msgid "No notifications"
msgstr "Geen meldingen"

#: src/ui/settings.ui:358
msgid "Clear all"
msgstr "Alles wissen"

#: src/ui/system-prompt.ui:62
msgid "Confirm:"
msgstr "Bevestigen:"

#: src/ui/top-panel.ui:15
msgid "Lock Screen"
msgstr "Vergrendelscherm"

#: src/ui/top-panel.ui:22
msgid "Logout"
msgstr "Afmelden"

#. Translators: This is a time format for a date in
#. long format
#: src/util.c:340
msgid "%A, %B %-e"
msgstr "%A %d %B"

#: src/vpn-info.c:89
msgid "VPN"
msgstr "VPN"

#: src/wifiinfo.c:90
msgid "Wi-Fi"
msgstr "Wifi"

#. Translators: Refers to the cellular wireless network
#: src/wwan-info.c:200
msgid "Cellular"
msgstr "Mobiel"

#~ msgid "Show only adaptive apps"
#~ msgstr "Enkel adaptieve toepassingen tonen"

#~ msgctxt ""
#~ "This is a monitor vendor name followed by product/model name where size "
#~ "in inches could not be calculated, e.g. Dell U2414H"
#~ msgid "%s %sn"
#~ msgstr "%s %sn"
