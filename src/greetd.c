/*
 * Copyright (C) 2022 Collabora Ltd
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Arnaud Ferraris <arnaud.ferraris@collabora.com>
 */

#define G_LOG_DOMAIN "phog-greetd"

#include "phog-config.h"

#include "greetd.h"
#include "greetd-session.h"

#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <gio/gio.h>
#include <gio/gdesktopappinfo.h>
#include <gio/gunixsocketaddress.h>
#include <json-glib/json-glib.h>

#define DEFAULT_MIN_UID 1000
#define DEFAULT_MAX_UID 60000

#define WAYLAND_SESSION_FOLDER  "/usr/share/wayland-sessions"
#define X11_SESSION_FOLDER      "/usr/share/xsessions"

#define LAST_SESSION_FILE       "/var/lib/phog/last-session"

/**
 * SECTION:lockscreen
 * @short_description: The main lock screen
 * @Title: PhogGreetd
 *
 * The lock screen featuring the clock
 * and unlock keypad.
 *
 * # CSS nodes
 *
 * #PhogGreetd has a CSS name with the name phog-lockscreen.
 */


typedef enum {
  CREATE_SESSION = 0,
  AUTHENTICATE   = 1,
  START_SESSION  = 2,
  ERROR          = 3,
} PhogGreetdMessageType;

typedef struct _PhogGreetd
{
  GObject parent;
  GSocket *socket;

  GListStore *available_sessions;
  GHashTable *available_users;

  gchar      *last_session;
} PhogGreetd;

G_DEFINE_TYPE (PhogGreetd, phog_greetd, G_TYPE_OBJECT)

#define PAYLOAD_MAX_SIZE 512

typedef struct {
    guint32 len;
    gchar payload[PAYLOAD_MAX_SIZE];
} GreetdMessage;

static gboolean
is_same_session (gconstpointer data1, gconstpointer data2)
{
    g_return_val_if_fail(data1, FALSE);
    g_return_val_if_fail(data2, FALSE);

    if (g_str_equal(phog_greetd_session_get_id (data1), phog_greetd_session_get_id (data2)))
        return TRUE;

    return FALSE;
}

static guint
session_get_position (PhogGreetd *self, const gchar *session_id)
{
    g_autoptr (PhogGreetdSession) ref = NULL;
    guint pos = 0;

    g_return_val_if_fail(PHOG_IS_GREETD (self), G_MAXUINT);
    g_return_val_if_fail(G_IS_LIST_STORE (self->available_sessions), G_MAXUINT);

    ref = PHOG_GREETD_SESSION (phog_greetd_session_new());
    phog_greetd_session_set_id(ref, session_id);
    if (g_list_store_find_with_equal_func(self->available_sessions, ref, is_same_session, &pos))
        return pos;

    return G_MAXUINT;
}

static void
greetd_list_sessions_in_folder (PhogGreetd *self, const gchar *path, const gchar *type)
{
    g_autoptr (GFile) dir = g_file_new_for_path (path);
    g_autoptr (GFileEnumerator) enumerator = g_file_enumerate_children (dir,
                                                                        G_FILE_ATTRIBUTE_STANDARD_NAME,
                                                                        G_FILE_QUERY_INFO_NONE,
                                                                        NULL, NULL);
    GFileInfo *file_info;

    while ((file_info = g_file_enumerator_next_file (enumerator, NULL, NULL))) {
        g_autoptr (GDesktopAppInfo) session_info = NULL;
        g_autofree gchar *session_path = NULL;
        g_autofree gchar *session_id = NULL;
        g_autofree gchar *desktop_names = NULL;
        PhogGreetdSession *session;
        const gchar *name = g_file_info_get_name (file_info);

        if (!g_str_has_suffix (name, ".desktop"))
            continue;

        session_path = g_strdup_printf ("%s/%s", path, name);
        session_info = g_desktop_app_info_new_from_filename (session_path);

        if (!session_info) {
            g_message ("Unable to open %s session file!", session_path);
            continue;
        }

        session_id = g_strdup (name);
        *(session_id + strlen (name) - strlen (".desktop")) = 0;

        /* Don't add multiple sessions with the same ID */
        if (session_get_position (self, session_id) != G_MAXUINT)
            continue;

        session = PHOG_GREETD_SESSION (phog_greetd_session_new());
        phog_greetd_session_set_id (session, session_id);
        phog_greetd_session_set_session_type (session, type);
        phog_greetd_session_set_name (session,
                                      g_app_info_get_name (G_APP_INFO (session_info)));
        phog_greetd_session_set_command (session,
                                         g_app_info_get_commandline (G_APP_INFO (session_info)));

        desktop_names = g_desktop_app_info_get_string (session_info, "DesktopNames");
        if (g_str_has_suffix (desktop_names, ";")) {
            desktop_names[strlen (desktop_names) - 1] = '\0';
        }
        desktop_names = g_strdelimit (desktop_names, ";", ':');
        phog_greetd_session_set_desktop_names (session, desktop_names ?: session_id);

        g_list_store_append (self->available_sessions, G_OBJECT (session));
        g_object_unref (G_OBJECT (session));
    }
}

static void
greetd_init_sessions_list (PhogGreetd *self)
{
    self->available_sessions = g_list_store_new (PHOG_TYPE_GREETD_SESSION);
    
    greetd_list_sessions_in_folder (self, WAYLAND_SESSION_FOLDER, "wayland");
    greetd_list_sessions_in_folder (self, X11_SESSION_FOLDER, "x11");

    if (g_file_get_contents(LAST_SESSION_FILE, &self->last_session, NULL, NULL))
        self->last_session = g_strstrip(self->last_session);
}

static void
greetd_init_users_list (PhogGreetd *self)
{
    g_autoptr (GFile) login_defs = g_file_new_for_path ("/etc/login.defs");
    guint min_uid = 0, max_uid = 0;

    self->available_users = g_hash_table_new_full (g_str_hash,
                                                   g_str_equal,
                                                   g_free,
                                                   g_free);

    if (login_defs) {
        g_autoptr (GFileInputStream) login_defs_stream = g_file_read (login_defs, NULL, NULL);
        if (login_defs_stream) {
            g_autoptr(GDataInputStream) read_stream =
                    g_data_input_stream_new (G_INPUT_STREAM (login_defs_stream));
            gchar *line = NULL;

            while ((line = g_data_input_stream_read_line (read_stream, NULL, NULL, NULL))) {
                if (g_str_has_prefix (line, "UID_MIN")) {
                    min_uid = strtoul (line + 7, NULL, 10);
                } else if (g_str_has_prefix (line, "UID_MAX")) {
                    max_uid = strtoul (line + 7, NULL, 10);
                }
            }
        }
    }

    if (!min_uid) {
        min_uid = DEFAULT_MIN_UID;
    } else if (!max_uid || (max_uid < min_uid)) {
        max_uid = DEFAULT_MAX_UID;
    }

    while (TRUE) {
        struct passwd *pw;
        gchar **gecos;

        pw = getpwent ();
        if (!pw)
            break;
        if (pw->pw_uid < min_uid || pw->pw_uid > max_uid)
            continue;

        gecos = g_strsplit(pw->pw_gecos, ",", 2);

        g_hash_table_insert (self->available_users,
                             g_strdup (pw->pw_name),
                             g_strdup (gecos[0]));

        g_strfreev (gecos);
    }
}

static void
phog_greetd_constructed (GObject *object)
{
    PhogGreetd *self = PHOG_GREETD (object);
    g_autoptr(GError) error = NULL;
    g_autoptr(GSocketAddress) addr = NULL;
    const gchar *socket_path = g_getenv("GREETD_SOCK");
    if (!socket_path) {
        g_critical("GREETD_SOCK is not set!");
        return;
    }

    self->socket = g_socket_new(G_SOCKET_FAMILY_UNIX, G_SOCKET_TYPE_STREAM, G_SOCKET_PROTOCOL_DEFAULT, &error);
    if (error) {
        g_critical("Unable to create GSocket: %s", error->message);
        return;
    }

    addr = g_unix_socket_address_new(socket_path);
    g_socket_set_blocking(self->socket, TRUE);
    g_socket_set_keepalive(self->socket, TRUE);
    g_socket_connect(self->socket, addr, NULL, &error);
    if (error) {
        g_critical("Unable to connect to greetd: %s", error->message);
        return;
    }
}


static void
phog_greetd_dispose (GObject *object)
{
  PhogGreetd *self = PHOG_GREETD (object);

  g_socket_close(self->socket, NULL);
  g_clear_object(&self->socket);

  g_list_store_remove_all (self->available_sessions);
  self->available_sessions = NULL;

  g_hash_table_destroy(self->available_users);
  self->available_users = NULL;

  G_OBJECT_CLASS (phog_greetd_parent_class)->dispose (object);
}


static void
phog_greetd_class_init (PhogGreetdClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)klass;

  object_class->constructed = phog_greetd_constructed;
  object_class->dispose = phog_greetd_dispose;
}


static void
phog_greetd_init (PhogGreetd *self)
{
    greetd_init_users_list (self);
    greetd_init_sessions_list (self);
}


GObject *
phog_greetd_new (void)
{
  return g_object_new (PHOG_TYPE_GREETD, NULL);
}

static JsonBuilder *
greetd_json_builder_new (const gchar *type)
{
    JsonBuilder *builder = json_builder_new ();

    json_builder_begin_object (builder);

    json_builder_set_member_name (builder, "type");
    json_builder_add_string_value (builder, type);

    return builder;
}

static gboolean
greetd_json_builder_end (PhogGreetd *self, JsonBuilder *builder, GError **error)
{
    g_autoptr(JsonParser) parser = json_parser_new ();
    g_autoptr(JsonGenerator) generator = json_generator_new ();
    g_autoptr(JsonNode) request_root = NULL;
    g_autoptr(JsonNode) reply_root = NULL;
    g_autofree gchar *data = NULL;
    JsonObject *root_object;
    const gchar *reply_type;
    GreetdMessage message;
    gsize length;

    json_builder_end_object (builder);
    request_root = json_builder_get_root (builder);

    json_generator_set_root (generator, request_root);
    data = json_generator_to_data (generator, &length);
    message.len = length;
    memcpy(message.payload, data, message.len);

    g_socket_send(self->socket, (const gchar *) &message, message.len + sizeof(guint32), NULL, error);
    if (*error)
        return FALSE;
    
    g_socket_receive(self->socket, (gchar *) &message, PAYLOAD_MAX_SIZE, NULL, error);
    if (*error)
        return FALSE;

    json_parser_load_from_data(parser, message.payload, (gsize) message.len, error);
    if (*error)
        return FALSE;

    reply_root = json_parser_get_root(parser);
    root_object = json_node_get_object(reply_root);
    reply_type = json_object_get_string_member(root_object, "type");
    if (g_str_equal(reply_type, "error")) {
        if (error) {
            *error = g_error_new (G_FILE_ERROR,
                                  G_FILE_ERROR_FAILED,
                                  "%s",
                                  json_object_get_string_member(root_object, "description"));
        }
        return FALSE;
    } else if (g_str_equal(reply_type, "auth_message")) {
        const char *auth_msg = json_object_get_string_member(root_object, "auth_message");
        reply_type = json_object_get_string_member(root_object, "auth_message_type");
        if (g_str_equal (reply_type, "error")) {
            if (error)
                *error = g_error_new (G_FILE_ERROR, G_FILE_ERROR_PERM, "%s", auth_msg);
            return FALSE;
        } else if (g_str_equal(reply_type, "info")) {
            g_message("Info: %s", auth_msg);
        }
    }

    return TRUE;
}

GHashTable *
phog_greetd_get_available_users (PhogGreetd *self)
{
    g_return_val_if_fail(PHOG_IS_GREETD (self), NULL);

    return self->available_users;
}

GListStore *
phog_greetd_get_available_sessions (PhogGreetd *self)
{
    g_return_val_if_fail(PHOG_IS_GREETD (self), NULL);

    return self->available_sessions;
}

guint
phog_greetd_get_last_session_idx (PhogGreetd *self)
{
    g_return_val_if_fail(PHOG_IS_GREETD (self), G_MAXUINT);
    g_return_val_if_fail(self->last_session, G_MAXUINT);

    return session_get_position(self, self->last_session);
}

gboolean
phog_greetd_create_session (PhogGreetd *self, const gchar * user, GError **error)
{
    g_autoptr(JsonBuilder) builder = greetd_json_builder_new ("create_session");

    json_builder_set_member_name (builder, "username");
    json_builder_add_string_value (builder, user);
    return greetd_json_builder_end (self, builder, error);
}

gboolean
phog_greetd_authenticate (PhogGreetd *self, const gchar *password, GError **error)
{
    g_autoptr(JsonBuilder) builder = greetd_json_builder_new ("post_auth_message_response");

    json_builder_set_member_name (builder, "response");
    json_builder_add_string_value (builder, password);

    return greetd_json_builder_end (self, builder, error);
}

gboolean
phog_greetd_start_session (PhogGreetd *self, gint session_pos, GError **error)
{
    g_autoptr(JsonBuilder) builder = greetd_json_builder_new ("start_session");
    PhogGreetdSession *session =
            PHOG_GREETD_SESSION (g_list_model_get_object (G_LIST_MODEL (self->available_sessions),
                                                                        session_pos));
    const gchar *session_id = phog_greetd_session_get_id (session);
    gchar *str;

    json_builder_set_member_name (builder, "cmd");
    json_builder_begin_array (builder);
    json_builder_add_string_value (builder,
                                   phog_greetd_session_get_command (session));
    json_builder_end_array (builder);

    // TODO: retrieve values from xsession/wayland session
    json_builder_set_member_name (builder, "env");
    json_builder_begin_array (builder);
    str = g_strdup_printf ("XDG_SESSION_TYPE=%s",
                           phog_greetd_session_get_session_type (session));
    json_builder_add_string_value (builder, str);
    g_free (str);
    str = g_strdup_printf ("XDG_CURRENT_DESKTOP=%s",
                           phog_greetd_session_get_desktop_names (session));
    json_builder_add_string_value (builder, str);
    g_free (str);
    str = g_strdup_printf ("XDG_SESSION_DESKTOP=%s", session_id);
    json_builder_add_string_value (builder, str);
    g_free (str);
    str = g_strdup_printf ("GDMSESSION=%s", session_id);
    json_builder_add_string_value (builder, str);
    g_free (str);
    json_builder_end_array (builder);

    if (!greetd_json_builder_end (self, builder, error))
        return FALSE;

    g_free (self->last_session);
    self->last_session = g_strdup (session_id);
    g_file_set_contents(LAST_SESSION_FILE, self->last_session, strlen (self->last_session), NULL);

    return TRUE;
}

gboolean
phog_greetd_cancel_session (PhogGreetd *self, GError **error)
{
    g_autoptr(JsonBuilder) builder = greetd_json_builder_new ("cancel_session");
    return greetd_json_builder_end (self, builder, error);
}
