/*
 * Copyright (C) 2021 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "layersurface.h"
#include "phoc-layer-shell-effects-unstable-v1-client-protocol.h"

G_BEGIN_DECLS

#define PHOG_TYPE_DRAG_SURFACE (phog_drag_surface_get_type ())

G_DECLARE_DERIVABLE_TYPE (PhogDragSurface, phog_drag_surface, PHOG, DRAG_SURFACE, PhogLayerSurface)

/**
 * PhogDragSurfaceState:
 * @PHOG_DRAG_SURFACE_STATE_FOLDED: Surface is folded.
 * @PHOG_DRAG_SURFACE_STATE_UNFOLDED: Surface is unfolded.
 * @PHOG_DRAG_SURFACE_STATE_DRAGGED: Surface is being dragged.
 *
 * The state of the drag surface.
 */
typedef enum _PhogDragSurfaceState {
  PHOG_DRAG_SURFACE_STATE_FOLDED,
  PHOG_DRAG_SURFACE_STATE_UNFOLDED,
  PHOG_DRAG_SURFACE_STATE_DRAGGED,
} PhogDragSurfaceState;

/**
 * PhogDragSurfaceDragMode:
 * @PHOG_DRAG_SURFACE_DRAG_MODE_FULL: Full surface is draggable
 * @PHOG_DRAG_SURFACE_DRAG_MODE_HANDLE: Handle area is draggable.
 * @PHOG_DRAG_SURFACE_DRAG_MODE_NONE: Surface is not draggable.
 *
 * The drag mode of the drag surface. Specifies how and where
 * the surface is draggable.
 */
typedef enum _PhogDragSurfaceDragMode {
  PHOG_DRAG_SURFACE_DRAG_MODE_FULL,
  PHOG_DRAG_SURFACE_DRAG_MODE_HANDLE,
  PHOG_DRAG_SURFACE_DRAG_MODE_NONE,
} PhogDragSurfaceDragMode;

/**
 * PhogDragSurfaceClass:
 * @parent_class: The parent class
 * @dragged: invoked when a surface is being dragged
 */
struct _PhogDragSurfaceClass {
  PhogLayerSurfaceClass parent_class;

  void                   (*dragged) (PhogDragSurface *self, int margin);
};

void                  phog_drag_surface_set_margin      (PhogDragSurface        *self,
                                                          int                      margin_folded,
                                                          int                      margin_unfolded);
float                 phog_drag_surface_get_threshold   (PhogDragSurface        *self);
void                  phog_drag_surface_set_threshold   (PhogDragSurface        *self,
                                                          double                   threshold);
PhogDragSurfaceState phog_drag_surface_get_drag_state  (PhogDragSurface        *self);
void                  phog_drag_surface_set_drag_state  (PhogDragSurface        *self,
                                                          PhogDragSurfaceState    state);
void                  phog_drag_surface_set_exclusive   (PhogDragSurface        *self,
                                                          guint                    exclusive);
PhogDragSurfaceDragMode phog_drag_surface_get_drag_mode (PhogDragSurface       *self);
void                  phog_drag_surface_set_drag_mode   (PhogDragSurface        *self,
                                                          PhogDragSurfaceDragMode mode);
guint                 phog_drag_surface_get_drag_handle (PhogDragSurface        *self);
void                  phog_drag_surface_set_drag_handle (PhogDragSurface        *self,
                                                          guint                    handle);

G_END_DECLS
