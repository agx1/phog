/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 *
 * Once based on maynard's panel which is
 * Copyright (C) 2014 Collabora Ltd. *
 * Author: Jonny Lamb <jonny.lamb@collabora.co.uk>
 */

#define G_LOG_DOMAIN "phog-shell"

#define WWAN_BACKEND_KEY "wwan-backend"

#include <stdlib.h>
#include <string.h>

#include <glib-object.h>
#include <glib-unix.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gdk/gdkwayland.h>

#include "phog-config.h"
#include "drag-surface.h"
#include "shell.h"
#include "batteryinfo.h"
#include "connectivity-info.h"
#include "fader.h"
#include "idle-manager.h"
#include "keyboard-events.h"
#include "lockscreen-manager.h"
#include "monitor-manager.h"
#include "monitor/monitor.h"
#include "settings.h"
#include "osk-manager.h"
#include "phog-wayland.h"
#include "phosh-private-client-protocol.h"
#include "top-panel.h"
#include "util.h"
#include "wifiinfo.h"
#include "wwan-info.h"
#include "wwan/phog-wwan-ofono.h"
#include "wwan/phog-wwan-mm.h"
#include "wwan/phog-wwan-backend.h"

/**
 * SECTION:shell
 * @short_description: The shell singleton
 * @Title: PhogShell
 *
 * #PhogShell is responsible for instantiating the GUI
 * parts of the shell#PhogTopPanel, #PhogHome,… and the managers that
 * interface with DBus #PhogMonitorManager, #PhogFeedbackManager, …
 * and coordinates between them.
 */

enum {
  PROP_0,
  PROP_LOCKED,
  PROP_BUILTIN_MONITOR,
  PROP_PRIMARY_MONITOR,
  PROP_SHELL_STATE,
  PROP_LAST_PROP
};
static GParamSpec *props[PROP_LAST_PROP];

enum {
  READY,
  N_SIGNALS
};
static guint signals[N_SIGNALS] = { 0 };

typedef struct
{
  PhogDragSurface *top_panel;
  GPtrArray *faders;              /* for final fade out */

  GtkWidget *notification_banner;

  PhogMonitor *primary_monitor;
  PhogMonitor *builtin_monitor;
  PhogMonitorManager *monitor_manager;
  PhogLockscreenManager *lockscreen_manager;
  PhogIdleManager *idle_manager;
  PhogOskManager  *osk_manager;
  PhogToplevelManager *toplevel_manager;
  PhogWifiManager *wifi_manager;
  PhogWWan *wwan;
  PhogKeyboardEvents *keyboard_events;

  PhogShellDebugFlags debug_flags;
  gboolean             startup_finished;
  guint                startup_finished_id;


  /* Mirrors PhogLockscreenManager's locked property */
  gboolean locked;

  PhogShellStateFlags shell_state;

  char           *theme_name;
  GtkCssProvider *css_provider;
} PhogShellPrivate;


typedef struct _PhogShell
{
  GObject parent;
} PhogShell;

G_DEFINE_TYPE_WITH_PRIVATE (PhogShell, phog_shell, G_TYPE_OBJECT)


static void
on_top_panel_activated (PhogShell    *self,
                        PhogTopPanel *window)
{
  PhogShellPrivate *priv = phog_shell_get_instance_private (self);

  g_return_if_fail (PHOG_IS_TOP_PANEL (priv->top_panel));
  phog_top_panel_toggle_fold (PHOG_TOP_PANEL(priv->top_panel));
}


static void
update_top_level_layer (PhogShell *self)
{
  PhogShellStateFlags state;
  PhogShellPrivate *priv;
  guint32 layer, current;
  gboolean use_top_layer;

  priv = phog_shell_get_instance_private (self);

  g_return_if_fail (PHOG_IS_SHELL (self));
  priv = phog_shell_get_instance_private (self);

  if (priv->top_panel == NULL)
    return;

  g_return_if_fail (PHOG_IS_TOP_PANEL (priv->top_panel));
  state = phog_shell_get_state (self);

  /* We want the top-bar on the lock screen */
  use_top_layer = !phog_shell_get_locked (self);
  if (use_top_layer)
    goto set_layer;

  /* If there's a modal dialog make sure it can extend over the top-panel */
  use_top_layer = !!(state & PHOG_STATE_MODAL_SYSTEM_PROMPT);

 set_layer:
  layer = use_top_layer ? ZWLR_LAYER_SHELL_V1_LAYER_TOP : ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY;
  g_object_get (priv->top_panel, "layer", &current, NULL);
  if (current == layer)
    return;

  g_debug ("Moving top-panel to %s layer", use_top_layer ? "top" : "overlay");
  phog_layer_surface_set_layer (PHOG_LAYER_SURFACE (priv->top_panel), layer);
  phog_layer_surface_wl_surface_commit (PHOG_LAYER_SURFACE (priv->top_panel));
}


static void
on_primary_monitor_configured (PhogShell *self, PhogMonitor *monitor)
{
  PhogShellPrivate *priv;
  int height;

  g_return_if_fail (PHOG_IS_SHELL (self));
  g_return_if_fail (PHOG_IS_MONITOR (monitor));
  priv = phog_shell_get_instance_private (self);

  phog_shell_get_area (self, NULL, &height);
  phog_layer_surface_set_size (PHOG_LAYER_SURFACE (priv->top_panel), -1, height);
}


static void
setup_primary_monitor_configured_handler (PhogShell *self)
{
  PhogShellPrivate *priv = phog_shell_get_instance_private (self);
  g_signal_connect_object (priv->primary_monitor, "configured",
                           G_CALLBACK (on_primary_monitor_configured),
                           self,
                           G_CONNECT_SWAPPED);

  if (phog_monitor_is_configured (priv->primary_monitor))
    on_primary_monitor_configured (self, priv->primary_monitor);
}

static void
panels_create (PhogShell *self)
{
  PhogShellPrivate *priv = phog_shell_get_instance_private (self);
  PhogMonitor *monitor;
  PhogWayland *wl = phog_wayland_get_default ();
  int height;
  guint32 top_layer;

  monitor = phog_shell_get_primary_monitor (self);
  g_return_if_fail (monitor);

  top_layer = priv->locked ? ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY : ZWLR_LAYER_SHELL_V1_LAYER_TOP;
  phog_shell_get_area (self, NULL, &height);
  priv->top_panel = PHOG_DRAG_SURFACE (phog_top_panel_new (
                                          phog_wayland_get_zwlr_layer_shell_v1 (wl),
                                          phog_wayland_get_zphoc_layer_shell_effects_v1 (wl),
                                          monitor->wl_output,
                                          top_layer,
                                          height));
  gtk_widget_show (GTK_WIDGET (priv->top_panel));

  g_signal_connect_swapped (
    priv->top_panel,
    "activated",
    G_CALLBACK (on_top_panel_activated),
    self);
}


static void
panels_dispose (PhogShell *self)
{
  PhogShellPrivate *priv = phog_shell_get_instance_private (self);

  g_clear_pointer (&priv->top_panel, phog_cp_widget_destroy);
}


/* Select proper style sheet in case of high contrast */
static void
on_gtk_theme_name_changed (PhogShell *self, GParamSpec *pspec, GtkSettings *settings)
{
  const char *style;
  g_autofree char *name = NULL;
  PhogShellPrivate *priv = phog_shell_get_instance_private (self);
  g_autoptr (GtkCssProvider) provider = gtk_css_provider_new ();

  g_object_get (settings, "gtk-theme-name", &name, NULL);

  if (g_strcmp0 (priv->theme_name, name) == 0)
    return;

  priv->theme_name = g_steal_pointer (&name);
  g_debug ("GTK theme: %s", priv->theme_name);

  if (priv->css_provider) {
    gtk_style_context_remove_provider_for_screen(gdk_screen_get_default (),
                                                 GTK_STYLE_PROVIDER (priv->css_provider));
  }

  style = phog_util_get_stylesheet (priv->theme_name);
  gtk_css_provider_load_from_resource (provider, style);
  gtk_style_context_add_provider_for_screen (gdk_screen_get_default (),
                                             GTK_STYLE_PROVIDER (provider),
                                             GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
  g_set_object (&priv->css_provider, provider);
}


static void
set_locked (PhogShell *self, gboolean locked)
{
  PhogShellPrivate *priv = phog_shell_get_instance_private(self);

  if (priv->locked == locked)
    return;

  priv->locked = locked;
  phog_shell_set_state (self, PHOG_STATE_LOCKED, priv->locked);
  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_LOCKED]);

  /* Hide settings on screen lock, otherwise the user just sees the settigns when
     unblanking the screen which can be confusing */
  if (priv->top_panel)
    phog_top_panel_fold (PHOG_TOP_PANEL (priv->top_panel));

  update_top_level_layer (self);
}


static void
phog_shell_set_property (GObject *object,
                          guint property_id,
                          const GValue *value,
                          GParamSpec *pspec)
{
  PhogShell *self = PHOG_SHELL (object);

  switch (property_id) {
  case PROP_LOCKED:
    /* Only written by lockscreen manager on property sync */
    set_locked (self, g_value_get_boolean (value));
    break;
  case PROP_PRIMARY_MONITOR:
    phog_shell_set_primary_monitor (self, g_value_get_object (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
phog_shell_get_property (GObject *object,
                          guint property_id,
                          GValue *value,
                          GParamSpec *pspec)
{
  PhogShell *self = PHOG_SHELL (object);
  PhogShellPrivate *priv = phog_shell_get_instance_private(self);

  switch (property_id) {
  case PROP_LOCKED:
    g_value_set_boolean (value, phog_shell_get_locked (self));
    break;
  case PROP_BUILTIN_MONITOR:
    g_value_set_object (value, phog_shell_get_builtin_monitor (self));
    break;
  case PROP_PRIMARY_MONITOR:
    g_value_set_object (value, phog_shell_get_primary_monitor (self));
    break;
  case PROP_SHELL_STATE:
    g_value_set_flags (value, priv->shell_state);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
phog_shell_dispose (GObject *object)
{
  PhogShell *self = PHOG_SHELL (object);
  PhogShellPrivate *priv = phog_shell_get_instance_private(self);

  g_clear_handle_id (&priv->startup_finished_id, g_source_remove);

  panels_dispose (self);
  g_clear_pointer (&priv->faders, g_ptr_array_unref);
  
  g_clear_object (&priv->wwan);
  g_clear_object (&priv->wifi_manager);
  g_clear_object (&priv->toplevel_manager);
  g_clear_object (&priv->osk_manager);
  g_clear_object (&priv->idle_manager);
  g_clear_object (&priv->lockscreen_manager);
  g_clear_object (&priv->monitor_manager);
  g_clear_object (&priv->builtin_monitor);
  g_clear_object (&priv->primary_monitor);
  g_clear_object (&priv->keyboard_events);

  g_clear_pointer (&priv->theme_name, g_free);
  g_clear_object (&priv->css_provider);

  G_OBJECT_CLASS (phog_shell_parent_class)->dispose (object);
}


static void
on_num_toplevels_changed (PhogShell *self, GParamSpec *pspec, PhogToplevelManager *toplevel_manager)
{
  g_return_if_fail (PHOG_IS_SHELL (self));
  g_return_if_fail (PHOG_IS_TOPLEVEL_MANAGER (toplevel_manager));
}


static void
on_toplevel_added (PhogShell *self, PhogToplevel *unused, PhogToplevelManager *toplevel_manager)
{
  g_return_if_fail (PHOG_IS_SHELL (self));
  g_return_if_fail (PHOG_IS_TOPLEVEL_MANAGER (toplevel_manager));
}


static gboolean
on_fade_out_timeout (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), G_SOURCE_REMOVE);

  priv = phog_shell_get_instance_private (self);

  /* kill all faders if we time out */
  priv->faders = g_ptr_array_remove_range (priv->faders, 0, priv->faders->len);

  return G_SOURCE_REMOVE;
}


static void
notify_compositor_up_state (PhogShell *self, enum phosh_private_shell_state state)
{
  struct phosh_private *phosh_private;

  g_debug ("Notify compositor state: %d", state);

  phosh_private = phog_wayland_get_phosh_private (phog_wayland_get_default ());
  if (phosh_private && phosh_private_get_version (phosh_private) >= PHOSH_PRIVATE_SHELL_READY_SINCE)
    phosh_private_set_shell_state (phosh_private, state);
}


static gboolean
on_startup_finished (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), G_SOURCE_REMOVE);
  priv = phog_shell_get_instance_private (self);

  notify_compositor_up_state (self, PHOSH_PRIVATE_SHELL_STATE_UP);

  priv->startup_finished_id = 0;
  return G_SOURCE_REMOVE;
}


static gboolean
setup_idle_cb (PhogShell *self)
{
  g_autoptr (GError) err = NULL;
  PhogShellPrivate *priv = phog_shell_get_instance_private (self);

  panels_create (self);

  g_signal_connect_object (priv->toplevel_manager,
                           "notify::num-toplevels",
                           G_CALLBACK(on_num_toplevels_changed),
                           self,
                           G_CONNECT_SWAPPED);
  on_num_toplevels_changed (self, NULL, priv->toplevel_manager);

  g_signal_connect_object (priv->toplevel_manager,
                           "toplevel-added",
                           G_CALLBACK(on_toplevel_added),
                           self,
                           G_CONNECT_SWAPPED);

  setup_primary_monitor_configured_handler (self);

  /* Delay signaling the compositor a bit so that idle handlers get a
   * chance to run and the user has can unlock right away. Ideally
   * we'd not need this */
  priv->startup_finished_id = g_timeout_add_seconds (1, (GSourceFunc)on_startup_finished, self);
  g_source_set_name_by_id (priv->startup_finished_id, "[phog] startup finished");

  priv->startup_finished = TRUE;
  g_signal_emit (self, signals[READY], 0);

  return FALSE;
}


/* Load all types that might be used in UI files */
static void
type_setup (void)
{
  g_type_ensure (PHOG_TYPE_BATTERY_INFO);
  g_type_ensure (PHOG_TYPE_CONNECTIVITY_INFO);
  g_type_ensure (PHOG_TYPE_SETTINGS);
  g_type_ensure (PHOG_TYPE_WIFI_INFO);
  g_type_ensure (PHOG_TYPE_WWAN_INFO);
}


static void
on_builtin_monitor_power_mode_changed (PhogShell *self, GParamSpec *pspec, PhogMonitor *monitor)
{
  PhogMonitorPowerSaveMode mode;
  PhogShellPrivate *priv;

  g_return_if_fail (PHOG_IS_SHELL (self));
  g_return_if_fail (PHOG_IS_MONITOR (monitor));
  priv = phog_shell_get_instance_private (self);

  g_object_get (monitor, "power-mode", &mode, NULL);
  /* Might be emitted on startup before lockscreen_manager is up */
  if (mode == PHOG_MONITOR_POWER_SAVE_MODE_OFF && priv->lockscreen_manager)
    phog_shell_lock (self);

  phog_shell_set_state (self, PHOG_STATE_BLANKED, mode == PHOG_MONITOR_POWER_SAVE_MODE_OFF);
}

static void
phog_shell_set_builtin_monitor (PhogShell *self, PhogMonitor *monitor)
{
  PhogShellPrivate *priv;

  g_return_if_fail (PHOG_IS_SHELL (self));
  g_return_if_fail (PHOG_IS_MONITOR (monitor) || monitor == NULL);
  priv = phog_shell_get_instance_private (self);

  if (priv->builtin_monitor == monitor)
    return;

  if (priv->builtin_monitor) {
    /* Power mode listener */
    g_signal_handlers_disconnect_by_func (priv->builtin_monitor,
                                          G_CALLBACK (on_builtin_monitor_power_mode_changed),
                                          self);
    g_clear_object (&priv->builtin_monitor);
  }

  g_debug ("New builtin monitor is %s", monitor ? monitor->name : "(none)");
  g_set_object (&priv->builtin_monitor, monitor);

  if (monitor) {
    g_signal_connect_swapped (priv->builtin_monitor,
                              "notify::power-mode",
                              G_CALLBACK (on_builtin_monitor_power_mode_changed),
                              self);
  }

  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_BUILTIN_MONITOR]);
}


/* Find a new builtin monitor that differs from old, otherwise NULL */
static PhogMonitor *
find_new_builtin_monitor (PhogShell *self, PhogMonitor *old)
{
  PhogShellPrivate *priv;
  PhogMonitor *monitor = NULL;

  g_return_val_if_fail (PHOG_IS_SHELL (self), NULL);
  priv = phog_shell_get_instance_private (self);

  for (int i = 0; i < phog_monitor_manager_get_num_monitors (priv->monitor_manager); i++) {
    PhogMonitor *tmp = phog_monitor_manager_get_monitor (priv->monitor_manager, i);
    if (phog_monitor_is_builtin (tmp) && tmp != old) {
      monitor = tmp;
      break;
    }
  }

  return monitor;
}


static void
on_monitor_added (PhogShell *self, PhogMonitor *monitor)
{
  PhogShellPrivate *priv;

  g_return_if_fail (PHOG_IS_SHELL (self));
  g_return_if_fail (PHOG_IS_MONITOR (monitor));
  priv = phog_shell_get_instance_private (self);

  g_debug ("Monitor %p (%s) added", monitor, monitor->name);

  /* Set built-in monitor if not set already */
  if (!priv->builtin_monitor && phog_monitor_is_builtin (monitor))
    phog_shell_set_builtin_monitor (self, monitor);

  /*
   * on_monitor_added() gets connected in phog_shell_constructed() but
   * we can't use phog_shell_set_primary_monitor() yet since the
   * shell object is not yet up and we can't move panels, etc. so
   * ignore that case. This is not a problem since phog_shell_constructed()
   * sets the primary monitor explicitly.
   */
  if (!priv->startup_finished)
    return;

  /* Set primary monitor if unset */
  if (priv->primary_monitor == NULL)
    phog_shell_set_primary_monitor (self, monitor);
}


static void
on_monitor_removed (PhogShell *self, PhogMonitor *monitor)
{
  PhogShellPrivate *priv;

  g_return_if_fail (PHOG_IS_SHELL (self));
  g_return_if_fail (PHOG_IS_MONITOR (monitor));
  priv = phog_shell_get_instance_private (self);

  if (priv->builtin_monitor == monitor) {
    PhogMonitor *new_builtin;

    g_debug ("Builtin monitor %p (%s) removed", monitor, monitor->name);

    new_builtin = find_new_builtin_monitor (self, monitor);
    phog_shell_set_builtin_monitor (self, new_builtin);
  }

  if (priv->primary_monitor == monitor) {
    g_debug ("Primary monitor %p (%s) removed", monitor, monitor->name);

    /* Prefer built in monitor when primary is gone... */
    if (priv->builtin_monitor) {
      phog_shell_set_primary_monitor (self, priv->builtin_monitor);
      return;
    }

    /* ...just pick the first one available otherwise */
    for (int i = 0; i < phog_monitor_manager_get_num_monitors (priv->monitor_manager); i++) {
      PhogMonitor *new_primary = phog_monitor_manager_get_monitor (priv->monitor_manager, i);
      if (new_primary != monitor) {
        phog_shell_set_primary_monitor (self, new_primary);
        break;
      }
    }

    /* We did not find another monitor so all monitors are gone */
    if (priv->primary_monitor == monitor) {
      g_debug ("All monitors gone");
      phog_shell_set_primary_monitor (self, NULL);
      return;
    }
  }
}


static void
phog_shell_constructed (GObject *object)
{
  PhogShell *self = PHOG_SHELL (object);
  PhogShellPrivate *priv = phog_shell_get_instance_private (self);

  G_OBJECT_CLASS (phog_shell_parent_class)->constructed (object);

  /* We bind this early since a wl_display_roundtrip () would make us miss
     existing toplevels */
  priv->toplevel_manager = phog_toplevel_manager_new ();

  priv->monitor_manager = phog_monitor_manager_new ();
  g_signal_connect_swapped (priv->monitor_manager,
                            "monitor-added",
                            G_CALLBACK (on_monitor_added),
                            self);
  g_signal_connect_swapped (priv->monitor_manager,
                            "monitor-removed",
                            G_CALLBACK (on_monitor_removed),
                            self);

  /* Make sure all outputs are up to date */
  phog_wayland_roundtrip (phog_wayland_get_default ());

  if (phog_monitor_manager_get_num_monitors (priv->monitor_manager)) {
    PhogMonitor *monitor = find_new_builtin_monitor (self, NULL);

    /* Setup builtin monitor if not set via 'monitor-added' */
    if (!priv->builtin_monitor && monitor) {
      phog_shell_set_builtin_monitor (self, monitor);
      g_debug ("Builtin monitor %p, configured: %d",
               priv->builtin_monitor,
               phog_monitor_is_configured (priv->builtin_monitor));
    }

    /* Setup primary monitor, prefer builtin */
    /* Can't invoke phog_shell_set_primary_monitor () since this involves
       updating the panels as well and we need to init more of the shell first */
    if (priv->builtin_monitor)
      priv->primary_monitor = g_object_ref (priv->builtin_monitor);
    else
      priv->primary_monitor = g_object_ref (phog_monitor_manager_get_monitor (priv->monitor_manager, 0));
  } else {
    g_error ("Need at least one monitor");
  }

  gtk_icon_theme_add_resource_path (gtk_icon_theme_get_default (),
                                    "/org/mobian/phog/icons");

  priv->lockscreen_manager = phog_lockscreen_manager_new ();
  g_object_bind_property (priv->lockscreen_manager, "locked",
                          self, "locked",
                          G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);

  priv->idle_manager = phog_idle_manager_get_default();

  priv->faders = g_ptr_array_new_with_free_func ((GDestroyNotify) (gtk_widget_destroy));

  priv->keyboard_events = phog_keyboard_events_new ();

  g_idle_add ((GSourceFunc) setup_idle_cb, self);
}


static void
phog_shell_class_init (PhogShellClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = phog_shell_constructed;
  object_class->dispose = phog_shell_dispose;

  object_class->set_property = phog_shell_set_property;
  object_class->get_property = phog_shell_get_property;

  type_setup ();

  /**
   * PhogShell:locked:
   *
   * Whether the screen is currently locked. This mirrors the property
   * from #PhogLockscreenManager for easier access.
   */
  props[PROP_LOCKED] =
    g_param_spec_boolean ("locked", "", "",
                          FALSE,
                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  /**
   * PhogShell:builtin-monitor:
   *
   * The built in monitor. This is a hardware property and hence can
   * only be read. It can be %NULL when not present or disabled.
   */
  props[PROP_BUILTIN_MONITOR] =
    g_param_spec_object ("builtin-monitor",
                         "Built in monitor",
                         "The builtin monitor",
                         PHOG_TYPE_MONITOR,
                         G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);
  /**
   * PhogShell:primary-monitor:
   *
   * The primary monitor that has the panels, lock screen etc.
   */
  props[PROP_PRIMARY_MONITOR] =
    g_param_spec_object ("primary-monitor",
                         "Primary monitor",
                         "The primary monitor",
                         PHOG_TYPE_MONITOR,
                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  props[PROP_SHELL_STATE] =
    g_param_spec_flags ("shell-state",
                        "Shell state",
                        "The state of the shell",
                        PHOG_TYPE_SHELL_STATE_FLAGS,
                        PHOG_STATE_NONE,
                        G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);

  signals[READY] = g_signal_new ("ready",
                                 G_TYPE_FROM_CLASS (klass),
                                 G_SIGNAL_RUN_LAST, 0,
                                 NULL, NULL, NULL,
                                 G_TYPE_NONE, 0);
}


static GDebugKey debug_keys[] =
{
 { .key = "always-splash",
   .value = PHOG_SHELL_DEBUG_FLAG_ALWAYS_SPLASH,
 },
};


static void
phog_shell_init (PhogShell *self)
{
  PhogShellPrivate *priv = phog_shell_get_instance_private (self);
  GtkSettings *gtk_settings;

  priv->debug_flags = g_parse_debug_string(g_getenv ("PHOG_DEBUG"),
                                           debug_keys,
                                           G_N_ELEMENTS (debug_keys));

  gtk_settings = gtk_settings_get_default ();
  g_object_set (G_OBJECT (gtk_settings), "gtk-application-prefer-dark-theme", TRUE, NULL);

  g_signal_connect_swapped (gtk_settings, "notify::gtk-theme-name", G_CALLBACK (on_gtk_theme_name_changed), self);
  on_gtk_theme_name_changed (self, NULL, gtk_settings);

  priv->shell_state = PHOG_STATE_NONE;
}


static gboolean
select_fallback_monitor (gpointer data)
{
  PhogShell *self = PHOG_SHELL (data);
  PhogShellPrivate *priv = phog_shell_get_instance_private (self);

  g_return_val_if_fail (PHOG_IS_MONITOR_MANAGER (priv->monitor_manager), FALSE);
  phog_monitor_manager_enable_fallback (priv->monitor_manager);

  return G_SOURCE_REMOVE;
}


void
phog_shell_set_primary_monitor (PhogShell *self, PhogMonitor *monitor)
{
  PhogShellPrivate *priv;
  PhogMonitor *m = NULL;
  gboolean needs_notify = FALSE;

  g_return_if_fail (PHOG_IS_MONITOR (monitor) || monitor == NULL);
  g_return_if_fail (PHOG_IS_SHELL (self));
  priv = phog_shell_get_instance_private (self);

  if (monitor == priv->primary_monitor)
    return;

  if (priv->primary_monitor)
    g_signal_handlers_disconnect_by_func (priv->builtin_monitor,
                                          G_CALLBACK (on_primary_monitor_configured),
                                          self);

  if (monitor != NULL) {
    /* Make sure the new monitor exists */
    for (int i = 0; i < phog_monitor_manager_get_num_monitors (priv->monitor_manager); i++) {
      m = phog_monitor_manager_get_monitor (priv->monitor_manager, i);
      if (monitor == m)
        break;
    }
    g_return_if_fail (monitor == m);
  }

  needs_notify = priv->primary_monitor == NULL;
  g_set_object (&priv->primary_monitor, monitor);
  g_debug ("New primary monitor is %s", monitor ? monitor->name : "(none)");

  /* Move panels to the new monitor by recreating the layer-shell surfaces */
  panels_dispose (self);
  if (monitor)
    panels_create (self);

  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_PRIMARY_MONITOR]);

  setup_primary_monitor_configured_handler (self);

  /* All monitors gone or disabled. See if monitor-manager finds a
   * fallback to enable. Do that in an idle callback so GTK can process
   * pending wayland events for the gone output */
  if (monitor == NULL) {
    /* No monitor we're not useful atm */
    notify_compositor_up_state (self, PHOSH_PRIVATE_SHELL_STATE_UNKNOWN);
    g_idle_add (select_fallback_monitor, self);
  } else {
    if (needs_notify)
      notify_compositor_up_state (self, PHOSH_PRIVATE_SHELL_STATE_UP);
  }
}


PhogMonitor *
phog_shell_get_builtin_monitor (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), NULL);
  priv = phog_shell_get_instance_private (self);
  g_return_val_if_fail (PHOG_IS_MONITOR (priv->builtin_monitor) || priv->builtin_monitor == NULL, NULL);

  return priv->builtin_monitor;
}


/**
 * phog_shell_get_primary_monitor:
 * @self: The shell
 *
 * Returns: the primary monitor or %NULL if there currently are no outputs
 */
PhogMonitor *
phog_shell_get_primary_monitor (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), NULL);
  priv = phog_shell_get_instance_private (self);

  return priv->primary_monitor;
}

/* Manager getters */


PhogLockscreenManager *
phog_shell_get_lockscreen_manager (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), NULL);
  priv = phog_shell_get_instance_private (self);

  g_return_val_if_fail (PHOG_IS_LOCKSCREEN_MANAGER (priv->lockscreen_manager), NULL);
  return priv->lockscreen_manager;
}


PhogMonitorManager *
phog_shell_get_monitor_manager (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), NULL);
  priv = phog_shell_get_instance_private (self);

  g_return_val_if_fail (PHOG_IS_MONITOR_MANAGER (priv->monitor_manager), NULL);
  return priv->monitor_manager;
}


PhogToplevelManager *
phog_shell_get_toplevel_manager (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), NULL);
  priv = phog_shell_get_instance_private (self);

  g_return_val_if_fail (PHOG_IS_TOPLEVEL_MANAGER (priv->toplevel_manager), NULL);
  return priv->toplevel_manager;
}


PhogOskManager *
phog_shell_get_osk_manager (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), NULL);
  priv = phog_shell_get_instance_private (self);

  if (!priv->osk_manager)
      priv->osk_manager = phog_osk_manager_new ();

  g_return_val_if_fail (PHOG_IS_OSK_MANAGER (priv->osk_manager), NULL);
  return priv->osk_manager;
}


PhogWifiManager *
phog_shell_get_wifi_manager (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), NULL);
  priv = phog_shell_get_instance_private (self);

  if (!priv->wifi_manager)
      priv->wifi_manager = phog_wifi_manager_new ();

  g_return_val_if_fail (PHOG_IS_WIFI_MANAGER (priv->wifi_manager), NULL);
  return priv->wifi_manager;
}


PhogWWan *
phog_shell_get_wwan (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), NULL);
  priv = phog_shell_get_instance_private (self);

  if (!priv->wwan) {
    g_autoptr (GSettings) settings = g_settings_new ("org.mobian.phog");
    PhogWWanBackend backend = g_settings_get_enum (settings, WWAN_BACKEND_KEY);

    switch (backend) {
      default:
      case PHOG_WWAN_BACKEND_MM:
        priv->wwan = PHOG_WWAN (phog_wwan_mm_new());
        break;
      case PHOG_WWAN_BACKEND_OFONO:
        priv->wwan = PHOG_WWAN (phog_wwan_ofono_new());
        break;
    }
  }

  g_return_val_if_fail (PHOG_IS_WWAN (priv->wwan), NULL);
  return priv->wwan;
}

/**
 * phog_shell_get_usable_area:
 * @self: The shell
 * @x:(out)(nullable): The x coordinate where client usable area starts
 * @y:(out)(nullable): The y coordinate where client usable area starts
 * @width:(out)(nullable): The width of the client usable area
 * @height:(out)(nullable): The height of the client usable area
 *
 * Gives the usable area in pixels usable by a client on the primary
 * display.
 */
void
phog_shell_get_usable_area (PhogShell *self, int *x, int *y, int *width, int *height)
{
  PhogMonitor *monitor;
  PhogMonitorMode *mode;
  int w, h;
  float scale;

  g_return_if_fail (PHOG_IS_SHELL (self));

  monitor = phog_shell_get_primary_monitor (self);
  g_return_if_fail(monitor);
  mode = phog_monitor_get_current_mode (monitor);
  g_return_if_fail (mode != NULL);

  scale = MAX(1.0, phog_monitor_get_fractional_scale (monitor));

  g_debug ("Primary monitor %p scale is %f, mode: %dx%d, transform is %d",
           monitor,
           scale,
           mode->width,
           mode->height,
           monitor->transform);

  switch (phog_monitor_get_transform(monitor)) {
  case PHOG_MONITOR_TRANSFORM_NORMAL:
  case PHOG_MONITOR_TRANSFORM_180:
  case PHOG_MONITOR_TRANSFORM_FLIPPED:
  case PHOG_MONITOR_TRANSFORM_FLIPPED_180:
    w = mode->width / scale;
    h = mode->height / scale - PHOG_TOP_PANEL_HEIGHT;// - PHOG_HOME_BUTTON_HEIGHT;
    break;
  default:
    w = mode->height / scale;
    h = mode->width / scale - PHOG_TOP_PANEL_HEIGHT;// - PHOG_HOME_BUTTON_HEIGHT;
    break;
  }

  if (x)
    *x = 0;
  if (y)
    *y = PHOG_TOP_PANEL_HEIGHT;
  if (width)
    *width = w;
  if (height)
    *height = h;
}

/**
 * phog_shell_get_area:
 * @self: The shell singleton
 * @width: (nullable): The available width
 * @height: (nullable): The available height
 *
 * Gives the currently available screen area on the primary display.
 */
void
phog_shell_get_area (PhogShell *self, int *width, int *height)
{
  int w, h;

  phog_shell_get_usable_area (self, NULL, NULL, &w, &h);

  if (width)
    *width = w;

  if (height)
    *height = h + PHOG_TOP_PANEL_HEIGHT;// + PHOG_HOME_BUTTON_HEIGHT;
}


PhogShell *
phog_shell_get_default (void)
{
  static PhogShell *instance;

  if (instance == NULL) {
    g_debug("Creating shell");
    instance = g_object_new (PHOG_TYPE_SHELL, NULL);
    g_object_add_weak_pointer (G_OBJECT (instance), (gpointer *)&instance);
  }
  return instance;
}

void
phog_shell_fade_out (PhogShell *self, guint timeout)
{
  PhogShellPrivate *priv;
  PhogMonitorManager *monitor_manager;

  g_debug ("Fading out...");
  g_return_if_fail (PHOG_IS_SHELL (self));
  monitor_manager = phog_shell_get_monitor_manager (self);
  g_return_if_fail (PHOG_IS_MONITOR_MANAGER (monitor_manager));
  priv = phog_shell_get_instance_private (self);

  for (int i = 0; i < phog_monitor_manager_get_num_monitors (monitor_manager); i++) {
    PhogFader *fader;
    PhogMonitor *monitor = phog_monitor_manager_get_monitor (monitor_manager, i);

    fader = phog_fader_new (monitor);
    g_ptr_array_add (priv->faders, fader);
    gtk_widget_show (GTK_WIDGET (fader));
    if (timeout > 0)
      g_timeout_add_seconds (timeout, (GSourceFunc) on_fade_out_timeout, self);
  }
}

/**
 * phog_shell_set_power_save:
 * @self: The shell
 * @enable: Wether power save mode should be enabled
 *
 * Enter power saving mode. This currently blanks all monitors.
 */
void
phog_shell_enable_power_save (PhogShell *self, gboolean enable)
{
  g_debug ("Entering power save mode");
  g_return_if_fail (PHOG_IS_SHELL (self));

  /*
   * Locking the outputs instructs g-s-d to tell us to put
   * outputs into power save mode via org.gnome.Mutter.DisplayConfig
   */
  phog_shell_set_locked(self, enable);

  /* TODO: other means of power saving */
}

/**
 * phog_shell_started_by_display_manager:
 * @self: The shell
 *
 * Returns: %TRUE if we were started from a display manager. %FALSE otherwise.
 */
gboolean
phog_shell_started_by_display_manager(PhogShell *self)
{
  g_return_val_if_fail (PHOG_IS_SHELL (self), FALSE);

  if (!g_strcmp0 (g_getenv ("GDMSESSION"), "phog"))
    return TRUE;

  return FALSE;
}

/**
 * phog_shell_is_startup_finished:
 * @self: The shell
 *
 * Returns: %TRUE if the shell finished startup. %FALSE otherwise.
 */
gboolean
phog_shell_is_startup_finished(PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), FALSE);
  priv = phog_shell_get_instance_private (self);

  return priv->startup_finished;
}


void
phog_shell_add_global_keyboard_action_entries (PhogShell *self,
                                                const GActionEntry *entries,
                                                gint n_entries,
                                                gpointer user_data)
{
  PhogShellPrivate *priv;

  g_return_if_fail (PHOG_IS_SHELL (self));
  priv = phog_shell_get_instance_private (self);
  g_return_if_fail (priv->keyboard_events);

  g_action_map_add_action_entries (G_ACTION_MAP (priv->keyboard_events),
                                   entries,
                                   n_entries,
                                   user_data);
}


void
phog_shell_remove_global_keyboard_action_entries (PhogShell *self,
                                                   GStrv       action_names)
{
  PhogShellPrivate *priv;

  g_return_if_fail (PHOG_IS_SHELL (self));
  priv = phog_shell_get_instance_private (self);
  g_return_if_fail (priv->keyboard_events);

  for (int i = 0; i < g_strv_length (action_names); i++) {
    g_action_map_remove_action (G_ACTION_MAP (priv->keyboard_events),
                                action_names[i]);
  }
}

/**
 * phog_shell_get_state
 * @self: The shell
 *
 * Returns: The current #PhogShellStateFlags
 */
PhogShellStateFlags
phog_shell_get_state (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), PHOG_STATE_NONE);
  priv = phog_shell_get_instance_private (self);

  return priv->shell_state;
}

/**
 * phog_shell_set_state:
 * @self: The shell
 * @state: The #PhogShellStateFlags to set
 * @enabled: %TRUE to set a shell state, %FALSE to reset
 *
 * Set the shells state.
 */
void
phog_shell_set_state (PhogShell          *self,
                       PhogShellStateFlags state,
                       gboolean             enabled)
{
  PhogShellPrivate *priv;
  PhogShellStateFlags old_state;
  g_autofree gchar *str_state = NULL;
  g_autofree gchar *str_new_flags = NULL;

  g_return_if_fail (PHOG_IS_SHELL (self));
  priv = phog_shell_get_instance_private (self);

  old_state = priv->shell_state;

  if (enabled)
    priv->shell_state = priv->shell_state | state;
  else
    priv->shell_state = priv->shell_state & ~state;

  if (old_state == priv->shell_state)
    return;

  str_state = g_flags_to_string (PHOG_TYPE_SHELL_STATE_FLAGS,
                                 state);
  str_new_flags = g_flags_to_string (PHOG_TYPE_SHELL_STATE_FLAGS,
                                     priv->shell_state);

  g_debug ("%s %s %s shells state. New state: %s",
           enabled ? "Adding" : "Removing",
           str_state,
           enabled ? "to" : "from", str_new_flags);

  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_SHELL_STATE]);

  if (state & PHOG_STATE_MODAL_SYSTEM_PROMPT)
    update_top_level_layer (self);
}

void
phog_shell_lock (PhogShell *self)
{
  g_return_if_fail (PHOG_IS_SHELL (self));

  phog_shell_set_locked (self, TRUE);
}


void
phog_shell_unlock (PhogShell *self)
{
  g_return_if_fail (PHOG_IS_SHELL (self));

  phog_shell_set_locked (self, FALSE);
}

/**
 * phog_shell_get_locked:
 * @self: The #PhogShell singleton
 *
 * Returns: %TRUE if the shell is currently locked, otherwise %FALSE.
 */
gboolean
phog_shell_get_locked (PhogShell *self)
{
  PhogShellPrivate *priv;

  g_return_val_if_fail (PHOG_IS_SHELL (self), FALSE);
  priv = phog_shell_get_instance_private (self);

  return priv->locked;
}

/**
 * phog_shell_set_locked:
 * @self: The #PhogShell singleton
 * @locked: %TRUE to lock the shell
 *
 * Lock the shell. We proxy to lockscreen-manager to avoid
 * that other parts of the shell need to care about this
 * abstraction.
 */
void
phog_shell_set_locked (PhogShell *self, gboolean locked)
{
  PhogShellPrivate *priv;

  g_return_if_fail (PHOG_IS_SHELL (self));
  priv = phog_shell_get_instance_private (self);

  if (locked == priv->locked)
    return;

  phog_lockscreen_manager_set_locked (priv->lockscreen_manager, locked);
}
