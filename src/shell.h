/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#pragma once

#include "lockscreen-manager.h"
#include "monitor-manager.h"
#include "monitor/monitor.h"
#include "osk-manager.h"
#include "toplevel-manager.h"
#include "wifimanager.h"
#include "wwan/phog-wwan-iface.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

/**
 * PhogShellStateFlags:
 * @PHOG_STATE_NONE: No other state
 * @PHOG_STATE_MODAL_SYSTEM_PROMPT: any modal prompt shown
 * @PHOG_STATE_BLANKED: built-in display off
 * @PHOG_STATE_LOCKED: displays locked
 * @PHOG_STATE_SETTINGS: settings menu unfolded from top bar
 * @PHOG_STATE_OVERVIEW: overview unfolded from bottom bar
 *
 * These flags are used to keep track of the state
 * the #PhogShell is in.
 */
typedef enum {
  PHOG_STATE_NONE                = 0,
  PHOG_STATE_MODAL_SYSTEM_PROMPT = 1 << 0,
  PHOG_STATE_BLANKED             = 1 << 1,
  PHOG_STATE_LOCKED              = 1 << 2,
  PHOG_STATE_SETTINGS            = 1 << 3,
  PHOG_STATE_OVERVIEW            = 1 << 4,
} PhogShellStateFlags;


/**
 * PhogShellDebugFlags
 * @PHOG_SHELL_DEBUG_FLAG_NONE: No debug flags
 * @PHOG_SHELL_DEBUG_FLAG_ALWAYS_SPLASH: always use splash (even when docked)

 * These flags are to enable/disable debugging features.
 */
typedef enum {
  PHOG_SHELL_DEBUG_FLAG_NONE          = 0,
  PHOG_SHELL_DEBUG_FLAG_ALWAYS_SPLASH = 1 << 1,
} PhogShellDebugFlags;


#define PHOG_TYPE_SHELL phog_shell_get_type()


G_DECLARE_FINAL_TYPE (PhogShell, phog_shell, PHOG, SHELL, GObject)

PhogShell          *phog_shell_get_default     (void);
void                 phog_shell_get_usable_area (PhogShell *self,
                                                  int        *x,
                                                  int        *y,
                                                  int        *width,
                                                  int        *height);
void                 phog_shell_get_area        (PhogShell *self, int *width, int *height);
void                 phog_shell_set_locked      (PhogShell *self, gboolean locked);
gboolean             phog_shell_get_locked      (PhogShell *self);
void                 phog_shell_lock            (PhogShell *self);
void                 phog_shell_unlock          (PhogShell *self);
void                 phog_shell_set_primary_monitor (PhogShell *self, PhogMonitor *monitor);
PhogMonitor        *phog_shell_get_primary_monitor (PhogShell *self);
PhogMonitor        *phog_shell_get_builtin_monitor (PhogShell *self);

/* Created by the shell on startup */
PhogLockscreenManager *phog_shell_get_lockscreen_manager (PhogShell *self);
PhogMonitorManager    *phog_shell_get_monitor_manager    (PhogShell *self);
PhogToplevelManager   *phog_shell_get_toplevel_manager   (PhogShell *self);
/* Created on the fly */
PhogOskManager        *phog_shell_get_osk_manager        (PhogShell *self);
PhogWifiManager       *phog_shell_get_wifi_manager       (PhogShell *self);
PhogWWan              *phog_shell_get_wwan               (PhogShell *self);

void                 phog_shell_fade_out (PhogShell *self, guint timeout);
void                 phog_shell_enable_power_save (PhogShell *self, gboolean enable);
gboolean             phog_shell_started_by_display_manager(PhogShell *self);
gboolean             phog_shell_is_startup_finished (PhogShell *self);
void                 phog_shell_add_global_keyboard_action_entries (PhogShell *self,
                                                                     const GActionEntry *actions,
                                                                     gint n_entries,
                                                                     gpointer user_data);
void                 phog_shell_remove_global_keyboard_action_entries (PhogShell *self,
                                                                        GStrv action_names);
PhogShellStateFlags phog_shell_get_state (PhogShell *self);
void                 phog_shell_set_state (PhogShell *self, PhogShellStateFlags state, gboolean enabled);
PhogShellDebugFlags phog_shell_get_debug_flags (PhogShell *self);
G_END_DECLS
