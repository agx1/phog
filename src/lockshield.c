/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#define G_LOG_DOMAIN "phog-lockshield"

#include "phog-config.h"

#include "lockshield.h"

/**
 * SECTION:lockshield
 * @short_description: Lock shield for non primary screens
 * @Title: PhogLockshield
 *
 * The #PhogLockshield is displayed on lock screens
 * which are not the primary one.
 */
struct _PhogLockshield
{
  PhogLayerSurface parent;
};

G_DEFINE_TYPE(PhogLockshield, phog_lockshield, PHOG_TYPE_LAYER_SURFACE)


static void
phog_lockshield_constructed (GObject *object)
{
  PhogLockshield *self = PHOG_LOCKSHIELD (object);

  G_OBJECT_CLASS (phog_lockshield_parent_class)->constructed (object);

  gtk_style_context_add_class (
      gtk_widget_get_style_context (GTK_WIDGET (self)),
      "phog-lockshield");
}


static void
phog_lockshield_class_init (PhogLockshieldClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)klass;

  object_class->constructed = phog_lockshield_constructed;
}


static void
phog_lockshield_init (PhogLockshield *self)
{
}


GtkWidget *
phog_lockshield_new (gpointer layer_shell,
                      gpointer wl_output)
{
  return g_object_new (PHOG_TYPE_LOCKSHIELD,
                       "layer-shell", layer_shell,
                       "wl-output", wl_output,
                       "anchor", ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                                 ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM |
                                 ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                                 ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT,
                       "layer", ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY,
                       "kbd-interactivity", FALSE,
                       "exclusive-zone", -1,
                       "namespace", "phog lockshield",
                       NULL);
}
