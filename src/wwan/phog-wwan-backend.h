/*
 * Copyright (C) 2020 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once

/**
 * PhogWWanBackend:
 * @PHOG_WWAN_BACKEND_MM: Use ModemManager
 * @PHOG_WWAN_BACKEND_OFONO: Use oFono
 *
 * Which WWAN backend to use.
 */
typedef enum /*< enum,prefix=PHOG >*/
{
  PHOG_WWAN_BACKEND_MM,    /*< nick=modemmanager >*/
  PHOG_WWAN_BACKEND_OFONO, /*< nick=ofono >*/
} PhogWWanBackend;
