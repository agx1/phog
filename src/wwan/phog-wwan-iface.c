/*
 * Copyright (C) 2018-2020 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#define G_LOG_DOMAIN "phog-wwan-iface"

#include "phog-wwan-iface.h"
#include "wwan-manager.h"

/**
 * SECTION:phog-wwan-iface
 * @short_description: Interface for modem handling
 * @Title: PhogWWanInterface
 *
 * A #PhogWWanInterface handles modem interaction such as getting
 * network information and signal strength.
 **/

G_DEFINE_INTERFACE (PhogWWan, phog_wwan, G_TYPE_OBJECT)

void
phog_wwan_default_init (PhogWWanInterface *iface)
{
  g_object_interface_install_property (
    iface,
    g_param_spec_int ("signal-quality",
                      "Signal quality",
                      "Signal quality in percent",
                      0, 100, 0,
                      G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_interface_install_property (
    iface,
    g_param_spec_string ("access-tec",
                         "Access technology",
                         "Network access technology",
                         NULL,
                         G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_interface_install_property (
    iface,
    g_param_spec_boolean ("unlocked",
                          "Modem unlocked",
                          "Modem is unlocked",
                          FALSE,
                          G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_interface_install_property (
    iface,
    g_param_spec_boolean ("sim",
                          "Modem sim present",
                          "Modem has a sim card inserted",
                          FALSE,
                          G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_interface_install_property (
    iface,
    g_param_spec_boolean ("present",
                          "Modem present",
                          "Whether there is a modem present",
                          FALSE,
                          G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_interface_install_property (
    iface,
    g_param_spec_boolean ("enabled",
                          "Modem enabled",
                          "Whether there modem is enabled",
                          FALSE,
                          G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY));

  g_object_interface_install_property (
    iface,
    g_param_spec_string ("operator",
                         "Operator name",
                         "The network operator name",
                         NULL,
                         G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY));
}


guint
phog_wwan_get_signal_quality (PhogWWan *self)
{
  PhogWWanInterface *iface;

  g_return_val_if_fail (PHOG_IS_WWAN (self), 0);

  iface = PHOG_WWAN_GET_IFACE (self);
  g_return_val_if_fail (iface->get_signal_quality != NULL, 0);
  return iface->get_signal_quality (self);
}


const char*
phog_wwan_get_access_tec (PhogWWan *self)
{
  PhogWWanInterface *iface;

  g_return_val_if_fail (PHOG_IS_WWAN (self), NULL);

  iface = PHOG_WWAN_GET_IFACE (self);
  g_return_val_if_fail (iface->get_access_tec != NULL, NULL);
  return iface->get_access_tec (self);

}


gboolean
phog_wwan_is_unlocked (PhogWWan *self)
{
  PhogWWanInterface *iface;

  g_return_val_if_fail (PHOG_IS_WWAN (self), FALSE);

  iface = PHOG_WWAN_GET_IFACE (self);
  g_return_val_if_fail (iface->is_unlocked != NULL, FALSE);
  return iface->is_unlocked (self);
}


gboolean
phog_wwan_has_sim (PhogWWan *self)
{
  PhogWWanInterface *iface;

  g_return_val_if_fail (PHOG_IS_WWAN (self), FALSE);

  iface = PHOG_WWAN_GET_IFACE (self);
  g_return_val_if_fail (iface->has_sim != NULL, FALSE);
  return iface->has_sim (self);
}


gboolean
phog_wwan_is_present (PhogWWan *self)
{
  PhogWWanInterface *iface;

  g_return_val_if_fail (PHOG_IS_WWAN (self), FALSE);

  iface = PHOG_WWAN_GET_IFACE (self);
  g_return_val_if_fail (iface->is_present != NULL, FALSE);
  return iface->is_present (self);
}


gboolean
phog_wwan_is_enabled (PhogWWan *self)
{
  PhogWWanInterface *iface;

  g_return_val_if_fail (PHOG_IS_WWAN (self), FALSE);

  iface = PHOG_WWAN_GET_IFACE (self);
  g_return_val_if_fail (iface->is_enabled != NULL, FALSE);
  return iface->is_enabled (self);
}


void
phog_wwan_set_enabled (PhogWWan *self, gboolean enabled)
{
  PhogWWanManager *manager;

  g_return_if_fail (PHOG_IS_WWAN_MANAGER (self));

  manager = PHOG_WWAN_MANAGER (self);
  phog_wwan_manager_set_enabled (manager, enabled);
}


const char *
phog_wwan_get_operator (PhogWWan *self)
{
  PhogWWanInterface *iface;

  g_return_val_if_fail (PHOG_IS_WWAN (self), NULL);

  iface = PHOG_WWAN_GET_IFACE (self);
  return iface->get_operator (self);
}
