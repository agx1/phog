/*
 * Copyright (C) 2021 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define PHOG_TYPE_WWAN_MANAGER (phog_wwan_manager_get_type())

G_DECLARE_DERIVABLE_TYPE (PhogWWanManager, phog_wwan_manager, PHOG, WWAN_MANAGER, GObject)

struct _PhogWWanManagerClass {
  GObjectClass parent_class;
};

PhogWWanManager  *phog_wwan_manager_new (void);
void               phog_wwan_manager_set_enabled (PhogWWanManager *self, gboolean enabled);
gboolean           phog_wwan_manager_get_data_enabled (PhogWWanManager *self);

G_END_DECLS
