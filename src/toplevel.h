/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "wlr-foreign-toplevel-management-unstable-v1-client-protocol.h"
#include <gtk/gtk.h>

#define PHOG_TYPE_TOPLEVEL (phog_toplevel_get_type())

G_DECLARE_FINAL_TYPE (PhogToplevel,
                      phog_toplevel,
                      PHOG,
                      TOPLEVEL,
                      GObject)

PhogToplevel *phog_toplevel_new_from_handle (struct zwlr_foreign_toplevel_handle_v1 *handle);
const char *phog_toplevel_get_title (PhogToplevel *self);
const char *phog_toplevel_get_app_id (PhogToplevel *self);
struct zwlr_foreign_toplevel_handle_v1 *phog_toplevel_get_handle (PhogToplevel *self);
gboolean phog_toplevel_is_configured (PhogToplevel *self);
gboolean phog_toplevel_is_activated (PhogToplevel *self);
gboolean phog_toplevel_is_maximized (PhogToplevel *self);
gboolean phog_toplevel_is_fullscreen (PhogToplevel *self);
void phog_toplevel_activate (PhogToplevel *self, struct wl_seat *seat);
void phog_toplevel_close (PhogToplevel *self);
