/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#define G_LOG_DOMAIN "phog-wwan-info"

#include <glib/gi18n.h>

#include "phog-config.h"
#include "wwan-info.h"
#include "wwan/wwan-manager.h"

#include "shell.h"

/**
 * SECTION:wwan-info
 * @short_description: A widget to display the wwan status
 * @Title: PhogWWanInfo
 *
 * A good indicator whether to show the icon is the
 * #PhogWWanInfo:present property that indicates if
 * hardware is present.
 */
enum {
  PROP_0,
  PROP_SHOW_DETAIL,
  PROP_PRESENT,
  PROP_LAST_PROP,
};
static GParamSpec *props[PROP_LAST_PROP];


struct _PhogWWanInfo
{
  GtkBox parent;

  PhogWWan *wwan;
  gboolean present;
  gboolean show_detail;
};

G_DEFINE_TYPE (PhogWWanInfo, phog_wwan_info, PHOG_TYPE_STATUS_ICON)

static void
phog_wwan_info_set_property (GObject *object,
                              guint property_id,
                              const GValue *value,
                              GParamSpec *pspec)
{
  PhogWWanInfo *self = PHOG_WWAN_INFO (object);

  switch (property_id) {
  case PROP_SHOW_DETAIL:
    phog_wwan_info_set_show_detail (self, g_value_get_boolean (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
phog_wwan_info_get_property (GObject *object,
                              guint property_id,
                              GValue *value,
                              GParamSpec *pspec)
{
  PhogWWanInfo *self = PHOG_WWAN_INFO (object);

  switch (property_id) {
  case PROP_SHOW_DETAIL:
    g_value_set_boolean (value, self->show_detail);
    break;
  case PROP_PRESENT:
    g_value_set_boolean (value, self->present);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


enum quality {
  QUALITY_EXCELLENT = 0,
  QUALITY_GOOD,
  QUALITY_OK,
  QUALITY_WEAK,
  QUALITY_NONE,
};

static const char *quality_data[] = {
  "network-cellular-signal-excellent-symbolic",
  "network-cellular-signal-good-symbolic",
  "network-cellular-signal-ok-symbolic",
  "network-cellular-signal-weak-symbolic",
  "network-cellular-signal-none-symbolic",
  NULL,
};

static const char *quality_no_data[] = {
  "network-cellular-no-data-signal-excellent-symbolic",
  "network-cellular-no-data-signal-good-symbolic",
  "network-cellular-no-data-signal-ok-symbolic",
  "network-cellular-no-data-signal-weak-symbolic",
  "network-cellular-no-data-signal-none-symbolic",
  NULL,
};

static const char *
signal_quality_icon_name (guint quality, gboolean data_enabled)
{
  const char **q = data_enabled ? quality_data : quality_no_data;

  if (quality > 80)
    return q[QUALITY_EXCELLENT];
  else if (quality > 55)
    return q[QUALITY_GOOD];
  else if (quality > 30)
    return q[QUALITY_OK];
  else if (quality > 5)
    return q[QUALITY_WEAK];
  else
    return q[QUALITY_NONE];
}


static void
update_icon_data(PhogWWanInfo *self, GParamSpec *psepc, PhogWWan *wwan)
{
  GtkWidget *access_tec_widget;
  guint quality;
  const char *icon_name = NULL;
  const char *access_tec;
  gboolean present, enabled, data_enabled;

  g_return_if_fail (PHOG_IS_WWAN_INFO (self));
  present = phog_wwan_is_present (self->wwan);
  g_debug ("Updating wwan present: %d", present);
  if (present != self->present) {
    self->present = present;
    g_object_notify_by_pspec (G_OBJECT (self), props[PROP_PRESENT]);
  }

  access_tec_widget = phog_status_icon_get_extra_widget (PHOG_STATUS_ICON (self));

  enabled = phog_wwan_is_enabled (self->wwan);
  if (!present) {
    icon_name = "network-cellular-disabled-symbolic";
  } else if (!phog_wwan_has_sim (self->wwan)) {
    icon_name = "auth-sim-missing-symbolic";
  } else if (!phog_wwan_is_unlocked (self->wwan)) {
      icon_name = "auth-sim-locked-symbolic";
  } else if (!enabled) {
    icon_name = "network-cellular-disabled-symbolic";
  }

  if (icon_name) {
    phog_status_icon_set_icon_name (PHOG_STATUS_ICON (self), icon_name);
    gtk_widget_hide (access_tec_widget);
    return;
  }

  /* Signal quality */
  quality = phog_wwan_get_signal_quality (self->wwan);
  data_enabled = phog_wwan_manager_get_data_enabled (PHOG_WWAN_MANAGER (self->wwan));
  icon_name = signal_quality_icon_name (quality, data_enabled);
  phog_status_icon_set_icon_name (PHOG_STATUS_ICON (self), icon_name);

  if (!self->show_detail) {
    gtk_widget_hide (access_tec_widget);
    return;
  }

  /* Access technology */
  access_tec = phog_wwan_get_access_tec (self->wwan);
  if (access_tec == NULL) {
    gtk_widget_hide (access_tec_widget);
    return;
  }

  gtk_label_set_text (GTK_LABEL (access_tec_widget), access_tec);
  gtk_widget_show (access_tec_widget);
}

static void
update_info (PhogWWanInfo *self)
{
  const char *info;
  g_return_if_fail (PHOG_IS_WWAN_INFO (self));

  info = phog_wwan_get_operator (self->wwan);
  if (!info || !g_strcmp0(info, "")) {
    /* Translators: Refers to the cellular wireless network */
    info = _("Cellular");
  }

  phog_status_icon_set_info (PHOG_STATUS_ICON (self), info);
}


static void
phog_wwan_info_idle_init (PhogStatusIcon *icon)
{
  PhogWWanInfo *self = PHOG_WWAN_INFO (icon);

  update_icon_data (self, NULL, NULL);
  update_info (self);
}


static void
phog_wwan_info_constructed (GObject *object)
{
  PhogWWanInfo *self = PHOG_WWAN_INFO (object);
  GStrv signals = (char *[]) {"notify::signal-quality",
                              "notify::access-tec",
                              "notify::unlocked",
                              "notify::sim",
                              "notify::present",
                              "notify::enabled",
                              "notify::data-enabled",
                              NULL,
  };

  G_OBJECT_CLASS (phog_wwan_info_parent_class)->constructed (object);

  self->wwan = g_object_ref (phog_shell_get_wwan (phog_shell_get_default ()));

  for (int i = 0; i < g_strv_length(signals); i++) {
    g_signal_connect_swapped (self->wwan, signals[i],
                              G_CALLBACK (update_icon_data),
                              self);
  }

  g_signal_connect_swapped (self->wwan,
                            "notify::operator",
                            G_CALLBACK (update_info),
                            self);
}


static void
phog_wwan_info_dispose (GObject *object)
{
  PhogWWanInfo *self = PHOG_WWAN_INFO(object);

  if (self->wwan) {
    g_signal_handlers_disconnect_by_data (self->wwan, self);
    g_clear_object (&self->wwan);
  }

  G_OBJECT_CLASS (phog_wwan_info_parent_class)->dispose (object);
}


static void
phog_wwan_info_class_init (PhogWWanInfoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  PhogStatusIconClass *status_icon_class = PHOG_STATUS_ICON_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = phog_wwan_info_set_property;
  object_class->get_property = phog_wwan_info_get_property;

  object_class->constructed = phog_wwan_info_constructed;
  object_class->dispose = phog_wwan_info_dispose;

  gtk_widget_class_set_css_name (widget_class, "phog-wwan-info");

  status_icon_class->idle_init = phog_wwan_info_idle_init;

  props[PROP_SHOW_DETAIL] =
    g_param_spec_boolean (
      "show-detail",
      "Show detail",
      "Show wwan details",
      FALSE,
      G_PARAM_CONSTRUCT | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  props[PROP_PRESENT] =
    g_param_spec_boolean (
      "present",
      "Present",
      "Whether WWAN hardware is present",
      FALSE,
      G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);
}


static void
phog_wwan_info_init (PhogWWanInfo *self)
{
  GtkWidget *access_tec = gtk_label_new (NULL);
  phog_status_icon_set_extra_widget (PHOG_STATUS_ICON (self), access_tec);
}


GtkWidget *
phog_wwan_info_new (void)
{
  return g_object_new (PHOG_TYPE_WWAN_INFO, NULL);
}


void
phog_wwan_info_set_show_detail (PhogWWanInfo *self, gboolean show)
{
  GtkWidget *access_tec;

  g_return_if_fail (PHOG_IS_WWAN_INFO (self));

  if (self->show_detail == show)
    return;

  self->show_detail = !!show;

  access_tec = phog_status_icon_get_extra_widget (PHOG_STATUS_ICON (self));
  gtk_widget_set_visible (access_tec, show);

  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_SHOW_DETAIL]);
}


gboolean
phog_wwan_info_get_show_detail (PhogWWanInfo *self)
{
  g_return_val_if_fail (PHOG_IS_WWAN_INFO (self), 0);

  return self->show_detail;
}
