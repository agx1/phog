/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include "status-icon.h"

G_BEGIN_DECLS

#define PHOG_TYPE_CONNECTIVITY_INFO (phog_connectivity_info_get_type())

G_DECLARE_FINAL_TYPE (PhogConnectivityInfo, phog_connectivity_info, PHOG, CONNECTIVITY_INFO, PhogStatusIcon)

GtkWidget * phog_connectivity_info_new (void);

G_END_DECLS
