/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Sebastian Krzyszkowiak <sebastian.krzyszkowiak@puri.sm>
 */

#include "toplevel-manager.h"
#include "toplevel.h"


enum {
  SIGNAL_TOPLEVEL_ADDED,
  SIGNAL_TOPLEVEL_CHANGED,
  N_SIGNALS
};
static guint signals[N_SIGNALS] = { 0 };

struct _PhogToplevelManager {
  GObject parent;
};

G_DEFINE_TYPE (PhogToplevelManager, phog_toplevel_manager, G_TYPE_OBJECT);


static void
phog_toplevel_manager_class_init (PhogToplevelManagerClass *klass)
{
  signals[SIGNAL_TOPLEVEL_ADDED] = g_signal_new (
    "toplevel-added",
    G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
    NULL, G_TYPE_NONE, 1, PHOG_TYPE_TOPLEVEL);
  signals[SIGNAL_TOPLEVEL_ADDED] = g_signal_new (
    "toplevel-changed",
    G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
    NULL, G_TYPE_NONE, 1, PHOG_TYPE_TOPLEVEL);
}


static void
phog_toplevel_manager_init (PhogToplevelManager *self)
{
}


PhogToplevel *
phog_toplevel_manager_get_toplevel (PhogToplevelManager *self, guint num)
{
  return phog_toplevel_new_from_handle (NULL);
}


guint
phog_toplevel_manager_get_num_toplevels (PhogToplevelManager *self)
{
  return 0;
}


PhogToplevelManager *
phog_toplevel_manager_new (void)
{
  return g_object_new (PHOG_TYPE_TOPLEVEL_MANAGER, NULL);
}
